package com.thomasdomingues.epihub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.thomasdomingues.epihub.services.CheckForNewNewsIntentService;

public class CheckNewNewsAlarmReceiver extends BroadcastReceiver {

    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.thomasdomingues.epihub.checkfornewnews.alarm";

    public CheckNewNewsAlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        CheckForNewNewsIntentService.startActionFoo(context);
    }
}
