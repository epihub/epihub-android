package com.thomasdomingues.epihub.network;

import com.thomasdomingues.epihub.models.ArticleModel;
import com.thomasdomingues.epihub.models.NewsgroupModel;

import org.apache.commons.net.nntp.Article;
import org.apache.commons.net.nntp.NNTPClient;
import org.apache.commons.net.nntp.NewsgroupInfo;

import java.io.IOException;
import java.util.ArrayList;

import javax.mail.internet.MimeUtility;

public class ArticlesInfoClient {
    private String news_server_hostname;
    private int news_server_port;

    public ArticlesInfoClient(String hostname, int port) {
        this.news_server_hostname = hostname;
        this.news_server_port = port;
    }

    public long fetchRemoteArticleCount(NewsgroupModel group) throws IOException {
        NNTPClient client = new NNTPClient();
        long count = 0;

        client.connect(news_server_hostname, news_server_port);

        for (NewsgroupInfo remoteGroup : client.iterateNewsgroups()) {

            if (!remoteGroup.getNewsgroup().equals(group.getName())) {
                continue;
            }

            count = remoteGroup.getArticleCountLong();
            break;
        }

        if (client.isConnected()) {
            client.disconnect();
        }

        return count;
    }

    public ArrayList<ArticleModel> fetchAllFromGroup(NewsgroupModel group) throws IOException {
        ArrayList<ArticleModel> articles = new ArrayList<>();
        NNTPClient client = new NNTPClient();

        System.setProperty("mail.mime.decodetext.strict", "false");

        client.connect(news_server_hostname, news_server_port);

        NewsgroupInfo newsgroup = new NewsgroupInfo();
        client.selectNewsgroup(group.getName(), newsgroup);

        for (Article a : client.iterateArticleInfo(newsgroup.getFirstArticleLong(), newsgroup.getLastArticleLong())) {
            String from = MimeUtility.decodeText(a.getFrom());
            String subject = MimeUtility.decodeText(a.getSubject());
            String date = MimeUtility.decodeText(a.getDate());

            ArticleModel article = new ArticleModel(a.getArticleNumberLong(), from, group, subject, date, null);
            article.save();
            articles.add(0, article);
        }

        if (client.isConnected()) {
            client.disconnect();
        }

        return articles;
    }

}
