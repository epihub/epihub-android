package com.thomasdomingues.epihub.network;

import com.thomasdomingues.epihub.models.ArticleModel;

import org.apache.commons.net.nntp.NNTPClient;
import org.apache.commons.net.nntp.NewsgroupInfo;

import java.io.BufferedReader;
import java.io.IOException;

public class ArticleDetailClient {
    private String news_server_hostname;
    private int news_server_port;

    public ArticleDetailClient(String hostname, int port) {
        this.news_server_hostname = hostname;
        this.news_server_port = port;
    }

    public ArticleModel fetch(ArticleModel article) throws IOException {
        NNTPClient client = new NNTPClient();

        client.connect(news_server_hostname, news_server_port);

        NewsgroupInfo newsgroup = new NewsgroupInfo();
        client.selectNewsgroup(article.getGroup().getName(), newsgroup);

        BufferedReader brBody = client.retrieveArticleBody(article.getArticleNumber());
        article.setBody("");
        String line;

        if (brBody != null) {
            while ((line = brBody.readLine()) != null) {
                article.setBody(article.getBody() + new String(line.getBytes("ISO-8859-1")) + '\n');
            }
            brBody.close();
        }

        article.save();

        if (client.isConnected()) {
            client.disconnect();
        }

        return article;
    }

}
