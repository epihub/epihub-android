package com.thomasdomingues.epihub.network;

import com.activeandroid.ActiveAndroid;
import com.thomasdomingues.epihub.models.NewsgroupModel;

import org.apache.commons.net.nntp.NNTPClient;
import org.apache.commons.net.nntp.NewsgroupInfo;

import java.io.IOException;
import java.util.ArrayList;

public class NewsgroupsClient {

    private static final String FETCH_ALL = "";

    private String news_server_hostname;
    private int news_server_port;

    public NewsgroupsClient(String hostname, int port) {
        this.news_server_hostname = hostname;
        this.news_server_port = port;
    }

    public ArrayList<NewsgroupModel> fetchAll() throws IOException {
        ArrayList<NewsgroupModel> groups = new ArrayList<>();
        NNTPClient client = new NNTPClient();

        client.connect(news_server_hostname, news_server_port);

        for (NewsgroupInfo n : client.iterateNewsgroups(FETCH_ALL)) {
            NewsgroupModel group = new NewsgroupModel(n.getNewsgroup(), n.getArticleCountLong(), false);
            groups.add(group);
        }

        // Bulk insert newsgroups into local database if not exists
        ActiveAndroid.beginTransaction();
        try {
            for (NewsgroupModel n : groups) {
                n.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }

        if (client.isConnected()) {
            client.disconnect();
        }

        return groups;
    }

}
