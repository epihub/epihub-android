package com.thomasdomingues.epihub.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.activeandroid.query.Select;
import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.activities.BaseActivity;
import com.thomasdomingues.epihub.models.NewsgroupModel;

import org.apache.commons.net.nntp.NNTPClient;
import org.apache.commons.net.nntp.NewsgroupInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CheckForNewNewsIntentService extends IntentService {

    private static final String ACTION_FETCH_NEW_NEWS = "com.thomasdomingues.epihub.services.action.FOO";

    public CheckForNewNewsIntentService() {
        super("CheckForNewNewsIntentService");
    }

    public static void startActionFoo(Context context) {
        Intent intent = new Intent(context, CheckForNewNewsIntentService.class);
        intent.setAction(ACTION_FETCH_NEW_NEWS);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Log.i(this.getClass().toString(), "Fetching new articles in background...");
            final String action = intent.getAction();

            if (ACTION_FETCH_NEW_NEWS.equals(action)) {
                handleFetchNewNews();
            }
        }
    }

    private void handleFetchNewNews() {
        List<NewsgroupModel> queryResults = new Select()
                .from(NewsgroupModel.class)
                .where("Subscribed = ?", true)
                .orderBy("Name ASC")
                .execute();

        ArrayList<NewsgroupModel> subscribedGroups = (ArrayList<NewsgroupModel>) queryResults;
        ArrayList<NewArticles> messages = new ArrayList<>();

        if (subscribedGroups.isEmpty()) {
            return;
        }

        NNTPClient client = new NNTPClient();

        try {
            client.connect(getResources().getString(R.string.news_server_host), Integer.valueOf(getResources().getString(R.string.news_server_port)));

            for (NewsgroupInfo remoteGroup : client.iterateNewsgroups()) {
                Log.i(this.getClass().toString(), "Checking group : " + remoteGroup.getNewsgroup());

                NewsgroupModel localGroup = null;
                boolean found = false;
                for (NewsgroupModel g : subscribedGroups) {
                    if (remoteGroup.getNewsgroup().equals(g.getName())) {
                        localGroup = g;
                        found = true;
                    }
                }
                if (!found) {
                    Log.i(this.getClass().toString(), "Not in subscribed groups !");
                    continue;
                }

                long count = remoteGroup.getArticleCountLong();

                Log.i(this.getClass().toString(), "Remote count : " + count);
                Log.i(this.getClass().toString(), "Local count : " + localGroup.getArticleCount());

                if (count > localGroup.getArticleCount()) {
                    messages.add(new NewArticles(localGroup.getName(), count - localGroup.getArticleCount()));
                    localGroup.setArticleCount(count);
                    localGroup.save();
                }
            }

            if (client.isConnected()) {
                client.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (messages.isEmpty()) {
            return;
        }

        int index = 0;
        for (NewArticles message : messages) {
            String body;
            index++;

            if (message.getCount() == 1) {
                body = message.getCount() + " new article on " + message.getNewsgroup();
            }
            else {
                body = message.getCount() + " new articles on " + message.getNewsgroup();
            }

            createNotification(42 + index,
                    R.mipmap.ic_launcher,
                    getResources().getString(R.string.new_articles_notification_title),
                    body);
        }
    }

    private void createNotification(int nId, int iconRes, String title, String body) {
        // First let's define the intent to trigger when notification is selected
        // Start out by creating a normal intent (in this case to open an activity)
        Intent intent = new Intent(this, BaseActivity.class);
        // Next, let's turn this into a PendingIntent using
        //   public static PendingIntent getActivity(Context context, int requestCode,
        //       Intent intent, int flags)
        int requestID = (int) System.currentTimeMillis(); //unique requestID to differentiate between various notification with same NotifId
        int flags = PendingIntent.FLAG_CANCEL_CURRENT; // cancel old intent and create new one
        PendingIntent pIntent = PendingIntent.getActivity(this, requestID, intent, flags);
        // Now we can attach this to the notification using setContentIntent
        Notification noti = new NotificationCompat.Builder(this)
                .setSmallIcon(iconRes)
                .setContentTitle(title)
                .setContentText(body)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();

        noti.defaults = Notification.DEFAULT_ALL;

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows to update the notification later on.
        mNotificationManager.notify(nId, noti);
    }

    private class NewArticles {
        private final String newsgroup;
        private final long count;

        public NewArticles(String newsgroup, long count) {
            this.newsgroup = newsgroup;
            this.count = count;
        }

        public long getCount() {
            return count;
        }

        public String getNewsgroup() {
            return newsgroup;
        }
    }
}
