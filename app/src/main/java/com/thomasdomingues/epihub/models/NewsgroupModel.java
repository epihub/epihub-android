package com.thomasdomingues.epihub.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.List;

@Table(name = "Newsgroups")
public class NewsgroupModel extends Model implements Serializable {

    @Column(name = "Name", unique = true, onUniqueConflict = Column.ConflictAction.IGNORE)
    private String name;

    @Column(name = "ArticleCount")
    private long articleCount;

    @Column(name = "Subscribed")
    private boolean subscribed;

    public NewsgroupModel() {
        super();
    }

    public NewsgroupModel(String name, long articleCount, boolean subscribed) {
        super();
        this.name = name;
        this.articleCount = articleCount;
        this.subscribed = subscribed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(long articleCount) {
        this.articleCount = articleCount;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public List<ArticleModel> items() {
        return getMany(ArticleModel.class, "Newsgroup");
    }
}
