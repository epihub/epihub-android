package com.thomasdomingues.epihub.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

@Table(name = "Articles")
public class ArticleModel extends Model implements Serializable {
    // This is the unique article id given by the server
    @Column(name = "article_number", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private long articleNumber;

    @Column(name = "Author")
    private String from;

    @Column(name = "Newsgroup", onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    private NewsgroupModel group;

    @Column(name = "Subject")
    private String subject;

    @Column(name = "Date")
    private String date;

    @Column(name = "Body")
    private String body;

    @Column(name = "Read")
    private boolean read;

    public ArticleModel() {
        super();
    }

    public ArticleModel(long articleNumber, String from, NewsgroupModel group, String subject, String date, String body) {
        super();
        this.articleNumber = articleNumber;
        this.from = from;
        this.group = group;
        this.subject = subject;
        this.date = date;
        this.body = body;
        this.read = false;
    }

    public long getArticleNumber() {
        return articleNumber;
    }

    public void setArticleNumber(long articleNumber) {
        this.articleNumber = articleNumber;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public NewsgroupModel getGroup() {
        return group;
    }

    public void setGroup(NewsgroupModel group) {
        this.group = group;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public static List<ArticleModel> getAll(NewsgroupModel group) {
        return new Select()
                .from(ArticleModel.class)
                .where("Newsgroup = ?", group.getId())
                .orderBy("article_number DESC")
                .execute();
    }
}
