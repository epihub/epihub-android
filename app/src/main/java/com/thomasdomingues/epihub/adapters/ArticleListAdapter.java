package com.thomasdomingues.epihub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.models.ArticleModel;

import java.util.ArrayList;

public class ArticleListAdapter extends ArrayAdapter<ArticleModel> {

    private LayoutInflater inflater;

    public ArticleListAdapter(Context context, ArrayList<ArticleModel> articles) {
        super(context, R.layout.fragment_article_list_item, articles);

        // Cache the LayoutInflate to avoid asking for a new one each time.
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Article to display
        ArticleModel article = getItem(position);

        // The child views in each row.
        TextView subject;
        TextView from;
        TextView date;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_article_list_item, parent, false);

            subject = (TextView) convertView.findViewById(R.id.tvSubject);
            from = (TextView) convertView.findViewById(R.id.tvFrom);
            date = (TextView) convertView.findViewById(R.id.tvDate);

            // Attach viewHolder to ListView item
            convertView.setTag(new ArticlesViewHolder(subject, from, date));
        }
        else {
            ArticlesViewHolder viewHolder = (ArticlesViewHolder) convertView.getTag();

            subject = viewHolder.getSubject();
            from = viewHolder.getFrom();
            date = viewHolder.getDate();
        }

        /* FIXME change layout if article has already been read */

        // Display article data
        subject.setText(article.getSubject());
        from.setText(article.getFrom());
        date.setText(article.getDate());

        // Return the completed view to render on screen
        return convertView;
    }

    // View lookup cache
    private static class ArticlesViewHolder {
        private TextView subject;
        private TextView from;
        private TextView date;

        public ArticlesViewHolder(TextView subject, TextView from, TextView date) {
            this.subject = subject;
            this.from = from;
            this.date = date;
        }

        public TextView getSubject() {
            return subject;
        }

        public void setSubject(TextView subject) {
            this.subject = subject;
        }

        public TextView getFrom() {
            return from;
        }

        public void setFrom(TextView from) {
            this.from = from;
        }

        public TextView getDate() {
            return date;
        }

        public void setDate(TextView date) {
            this.date = date;
        }
    }
}
