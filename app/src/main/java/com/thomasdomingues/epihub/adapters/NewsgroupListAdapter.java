package com.thomasdomingues.epihub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.models.NewsgroupModel;

import java.util.ArrayList;

public class NewsgroupListAdapter extends ArrayAdapter<NewsgroupModel>{
    // View lookup cache
    private static class ViewHolder {
        TextView unread;
        TextView name;
        TextView count;
    }

    public NewsgroupListAdapter(Context context, ArrayList<NewsgroupModel> groups) {
        super(context, R.layout.fragment_newsgroup_list_item, groups);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        NewsgroupModel group = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_newsgroup_list_item, parent, false);
            viewHolder.unread = (TextView) convertView.findViewById(R.id.tvUnreadCount);
            viewHolder.name = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.count = (TextView) convertView.findViewById(R.id.tvCount);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.unread.setText("0");
        viewHolder.name.setText(group.getName());
        viewHolder.count.setText(String.valueOf(group.getArticleCount()));
        // Return the completed view to render on screen
        return convertView;
    }
}
