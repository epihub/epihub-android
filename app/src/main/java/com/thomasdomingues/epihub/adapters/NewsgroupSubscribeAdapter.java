package com.thomasdomingues.epihub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.models.NewsgroupModel;

import java.util.List;

public class NewsgroupSubscribeAdapter extends ArrayAdapter<NewsgroupModel> {

    private LayoutInflater inflater;

    public NewsgroupSubscribeAdapter(Context context, List<NewsgroupModel> groups) {
        super(context, R.layout.fragment_newsgroup_subscribe_item, groups);

        // Cache the LayoutInflate to avoid asking for a new one each time.
        inflater = LayoutInflater.from(context) ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Newsgroup to display
        NewsgroupModel group = this.getItem(position);

        // The child views in each row.
        TextView name;
        CheckBox subscribe;

        // Check if an existing view is being reused, otherwise inflate the view
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.fragment_newsgroup_subscribe_item, parent, false);

            // Create a view holding the ListView item child Views
            name = (TextView) convertView.findViewById(R.id.tvNameSubscribe);
            subscribe = (CheckBox) convertView.findViewById(R.id.chkSubscribe);

            // Attach viewHolder to ListView item
            convertView.setTag(new SubscribedNewsgroupViewHolder(name, subscribe));

            /* CUSTOM */
            // Create listener for LisView item's Checkbox
            subscribe.setOnClickListener( new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v ;
                    NewsgroupModel group = (NewsgroupModel) cb.getTag();
                    group.setSubscribed(!group.isSubscribed());
                    group.save();
                    cb.setChecked(group.isSubscribed());
                }
            });
        }
        else {
            SubscribedNewsgroupViewHolder viewHolder = (SubscribedNewsgroupViewHolder) convertView.getTag();
            subscribe = viewHolder.getSubscribe();
            name = viewHolder.getName();
        }

        /* CUSTOM */
        // Tag the CheckBox with the Newsgroup it is displaying, so that we can
        // access the group in onClick() when the CheckBox is toggled.
        subscribe.setTag(group);

        // Display newsgroup data
        subscribe.setChecked(group.isSubscribed());
        name.setText(group.getName());

        // Return the completed view to render on screen
        return convertView;
    }

    // ViewHolder for subscribed newsgroups list items
    // View lookup cache
    public static class SubscribedNewsgroupViewHolder {
        TextView name;
        CheckBox subscribe;

        public SubscribedNewsgroupViewHolder(TextView name, CheckBox subscribe) {
            this.name = name;
            this.subscribe = subscribe;
        }

        public CheckBox getSubscribe() {
            return subscribe;
        }

        public void setSubscribe(CheckBox subscribe) {
            this.subscribe = subscribe;
        }

        public TextView getName() {
            return name;
        }

        public void setName(TextView name) {
            this.name = name;
        }
    }
}
