package com.thomasdomingues.epihub.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.thomasdomingues.epihub.BuildConfig;
import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.fragments.news.articles.ArticleDetailFragment;
import com.thomasdomingues.epihub.fragments.news.articles.ArticleListFragment;
import com.thomasdomingues.epihub.fragments.news.newsgroups.NewsgroupListFragment;
import com.thomasdomingues.epihub.fragments.news.newsgroups.NewsgroupSubscribeFragment;
import com.thomasdomingues.epihub.fragments.settings.SettingsFragment;
import com.thomasdomingues.epihub.models.ArticleModel;
import com.thomasdomingues.epihub.models.NewsgroupModel;
import com.thomasdomingues.epihub.receivers.CheckNewNewsAlarmReceiver;

import java.util.Date;

public class BaseActivity extends AppCompatActivity implements
        NewsgroupListFragment.OnListFragmentInteractionListener,
        NewsgroupSubscribeFragment.OnListFragmentInteractionListener,
        ArticleListFragment.OnListFragmentInteractionListener,
        ArticleDetailFragment.OnFragmentInteractionListener {
    private DrawerLayout mDrawer;
    private NavigationView mNvDrawer;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        mToolbar = (Toolbar) findViewById(R.id.common_toolbar);
        setSupportActionBar(mToolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.common_drawer_layout);
        mDrawerToggle = setupDrawerToggle();
        mDrawerToggle.setToolbarNavigationClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
        mDrawer.setDrawerListener(mDrawerToggle);

        // Find our drawer view
        mNvDrawer = (NavigationView) findViewById(R.id.common_drawer_view);
        // Setup drawer view
        setupDrawerContent(mNvDrawer);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content, NewsgroupListFragment.newInstance()).commit();
        setTitle(getResources().getString(R.string.news_activity));

        scheduleAlarm();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            // action home should open or close the drawer
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.drawer_open, R.string.drawer_close);
    }

    public void setDrawerIconEnabled(boolean enabled) {
        mDrawerToggle.setDrawerIndicatorEnabled(enabled);
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
        Class fragmentClass = null;
        boolean available = true;

        switch (menuItem.getItemId()) {
            case R.id.nav_chronos:
                Toast.makeText(this, "Chronos page available soon!", Toast.LENGTH_SHORT)
                        .show();
                available = false;
                break;
            case R.id.nav_news:
                fragmentClass = NewsgroupListFragment.class;
                break;
            case R.id.nav_settings:
                fragmentClass = SettingsFragment.class;
                break;
            case R.id.nav_about:
                Toast.makeText(this, "About page available soon!", Toast.LENGTH_SHORT)
                        .show();
                available = false;
                break;
            default:
                break;
        }

        if (available) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_content, fragment).commit();

            // Highlight the selected item, update the title, and close the drawer
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
            mDrawer.closeDrawers();
        }
    }

    public void scheduleAlarm() {
        SharedPreferences preferences = this.getSharedPreferences(
                getString(R.string.news_preference_file_key), Context.MODE_PRIVATE);
        final SharedPreferences defaultPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Date lastNewsUpdateDate = new Date(
                preferences.getLong(
                        getResources().getString(R.string.saved_last_news_global_update),
                        getResources().getInteger(R.integer.saved_last_news_global_update_default)
                )
        );

        Log.d(this.getClass().getCanonicalName(), "news server hostname: " + defaultPreferences.getString(
                "nntp_server_host", "none"
        ));
        Log.d(this.getClass().getCanonicalName(), "news server port: " + defaultPreferences.getString(
                "nntp_server_port", "none"
        ));
        Log.d(this.getClass().getCanonicalName(), "news sync frequency (in sec.): " + defaultPreferences.getString(
                "news_sync_frequency", "none"
        ));

        // Construct an intent that will execute the AlarmReceiver
        Intent intent = new Intent(getApplicationContext(), CheckNewNewsAlarmReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, CheckNewNewsAlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Setup periodic alarm every 5 seconds
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
        // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
        // Interval set to 3 minutes
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES / 5, pIntent);
    }

    public void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), CheckNewNewsAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, CheckNewNewsAlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            setDrawerIconEnabled(false);
            fm.popBackStack();
            if (BuildConfig.DEBUG) {
                Log.i("MainActivity", "backstack count : " + fm.getBackStackEntryCount());
            }
            if (fm.getBackStackEntryCount() == 1) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                setDrawerIconEnabled(true);
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.i("MainActivity", "nothing on backstack, calling super");
            }
            super.onBackPressed();
        }
    }

    @Override
    public void onListFragmentInteraction(NewsgroupModel group) {
        if (BuildConfig.DEBUG) {
            Log.i(BaseActivity.class.toString(), "Newsgroup selected: " + group.getName());
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ArticleListFragment fragmentArticleList = ArticleListFragment.newInstance(group);

        setDrawerIconEnabled(false);

        ft.replace(R.id.fragment_content, fragmentArticleList);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onListFragmentInteraction(ArticleModel article) {
        if (BuildConfig.DEBUG) {
            Log.i(BaseActivity.class.toString(), "Article selected: " + article.getArticleNumber());
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ArticleDetailFragment fragmentArticleDetail = ArticleDetailFragment.newInstance(article);
        ft.replace(R.id.fragment_content, fragmentArticleDetail);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // Do nothing
    }
}
