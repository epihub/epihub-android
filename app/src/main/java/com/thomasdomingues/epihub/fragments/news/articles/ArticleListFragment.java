package com.thomasdomingues.epihub.fragments.news.articles;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.thomasdomingues.epihub.BuildConfig;
import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.activities.BaseActivity;
import com.thomasdomingues.epihub.adapters.ArticleListAdapter;
import com.thomasdomingues.epihub.fragments.news.newsgroups.NewsgroupListFragment;
import com.thomasdomingues.epihub.models.ArticleModel;
import com.thomasdomingues.epihub.models.NewsgroupModel;
import com.thomasdomingues.epihub.network.ArticlesInfoClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link ListFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ArticleListFragment.OnListFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ArticleListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleListFragment extends ListFragment {
    public static final String ARTICLE_FETCH_NORMAL = "normal";
    public static final String ARTICLE_FETCH_FORCE = "force";

    private NewsgroupModel mGroup;
    private ArrayList<ArticleModel> mArticles;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ArticleListFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ArticleListFragment.
     */
    public static ArticleListFragment newInstance(NewsgroupModel group) {
        ArticleListFragment fragment = new ArticleListFragment();
        Bundle args = new Bundle();
        args.putSerializable("group", group);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mGroup = (NewsgroupModel) getArguments().getSerializable("group");

        mArticles = new ArrayList<>();
        setListAdapter(new ArticleListAdapter(getActivity(), mArticles));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup parent = (ViewGroup) inflater.inflate(R.layout.fragment_article_list, container, false);
        parent.addView(v, 0);
        return parent;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Set the adapter
        ListView lv = (ListView) view.findViewById(android.R.id.list);
        TextView emptyText = (TextView) view.findViewById(android.R.id.empty);
        lv.setEmptyView(emptyText);
        lv.setAdapter(getListAdapter());
        fetchArticlesAsync(ARTICLE_FETCH_NORMAL);
        getActivity().setTitle(mGroup.getName());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        mListener.onListFragmentInteraction(mArticles.get(position));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Get item selected and deal with it
        switch (item.getItemId()) {
            case android.R.id.home:
                //called when the up affordance/carat in actionbar is pressed
                getActivity().onBackPressed();
                return true;
            default:
                return true;
        }
    }

    private void fetchArticlesAsync(String forced) {
        String news_server_hostname = getResources().getString(R.string.news_server_host);
        String news_server_port = getResources().getString(R.string.news_server_port);
        if (forced.equals(ARTICLE_FETCH_FORCE)) {
            new FetchArticlesAsyncTask().execute(news_server_hostname, news_server_port, "true");
        }
        else {
            new FetchArticlesAsyncTask().execute(news_server_hostname, news_server_port, "false");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(ArticleModel article);
    }

    private class FetchArticlesAsyncTask extends AsyncTask<String, Void, ArrayList<ArticleModel>> {
        protected void onPreExecute() {
            // Runs on the UI thread before doInBackground
            // Good for toggling visibility of a progress indicator
            setListShown(false);
        }

        protected ArrayList<ArticleModel> doInBackground(String... strings) {
            // Some long-running task like downloading an image.
            try {
                // TODO Fetch only if remote count > local count
                ArticlesInfoClient client = new ArticlesInfoClient(strings[0], Integer.parseInt(strings[1]));

                List<ArticleModel> queryResults = ArticleModel.getAll(mGroup);
                long remoteCount = client.fetchRemoteArticleCount(mGroup);

                if (Boolean.parseBoolean(strings[2]) || queryResults.size() != remoteCount) {
                    if (BuildConfig.DEBUG) {
                        Log.d(NewsgroupListFragment.class.toString(), "Fetching articles from server...");
                    }
                    return client.fetchAllFromGroup(mGroup);
                }
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Fetching articles locally...");
                }
                return (ArrayList<ArticleModel>) queryResults;
            }
            catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Error while fetching articles from server:");
                }
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(ArrayList<ArticleModel> articles) {
            // This method is executed in the UIThread
            // with access to the result of the long running task
            if (null != articles) {
                mArticles = articles;
            }
            else {
                mArticles = new ArrayList<>();
            }
            super.onPostExecute(articles);
            ArticleListAdapter adapter = (ArticleListAdapter) getListAdapter();
            adapter.clear();
            adapter.addAll(mArticles);
            adapter.notifyDataSetChanged();
            setListAdapter(adapter);
            setListShown(true);
        }
    }
}
