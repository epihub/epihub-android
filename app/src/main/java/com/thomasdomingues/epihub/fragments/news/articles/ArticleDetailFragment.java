package com.thomasdomingues.epihub.fragments.news.articles;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thomasdomingues.epihub.BuildConfig;
import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.activities.BaseActivity;
import com.thomasdomingues.epihub.fragments.news.newsgroups.NewsgroupListFragment;
import com.thomasdomingues.epihub.models.ArticleModel;
import com.thomasdomingues.epihub.network.ArticleDetailClient;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ArticleDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ArticleDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleDetailFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ARTICLE = "article";

    public static final String ARTICLE_BODY_FETCH_NORMAL = "normal";
    public static final String ARTICLE_BODY_FETCH_FORCE = "force";

    private ArticleModel mArticle;

    private View mLayout;
    private TextView tvSubject;
    private TextView tvFrom;
    private TextView tvDate;
    private TextView tvBody;

    private OnFragmentInteractionListener mListener;

    public ArticleDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param article The article to show.
     * @return A new instance of fragment ArticleDetailFragment.
     */
    public static ArticleDetailFragment newInstance(ArticleModel article) {
        ArticleDetailFragment fragment = new ArticleDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ARTICLE, article);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        ((BaseActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getArguments() != null) {
            mArticle = (ArticleModel) getArguments().getSerializable(ARG_ARTICLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mLayout = view;
        tvSubject = (TextView) view.findViewById(R.id.tvArticleSubject);
        tvFrom = (TextView) view.findViewById(R.id.tvArticleFrom);
        tvDate = (TextView) view.findViewById(R.id.tvArticleDate);
        tvBody = (TextView) view.findViewById(R.id.tvArticleBody);
        fetchArticleBodyAsync(ARTICLE_BODY_FETCH_NORMAL);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Get item selected and deal with it
        switch (item.getItemId()) {
            case android.R.id.home:
                //called when the up affordance/carat in actionbar is pressed
                getActivity().onBackPressed();
                return true;
            default:
                return true;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void showArticleBodyLoading(boolean state) {
        if (mLayout != null) {
            if (state) {
                mLayout.findViewById(R.id.loadingArticleBodyPanel).setVisibility(View.VISIBLE);
            }
            else {
                mLayout.findViewById(R.id.loadingArticleBodyPanel).setVisibility(View.GONE);
            }
        }
    }

    private void fetchArticleBodyAsync(String forced) {
        String news_server_hostname = getResources().getString(R.string.news_server_host);
        String news_server_port = getResources().getString(R.string.news_server_port);
        if (forced.equals(ARTICLE_BODY_FETCH_FORCE)) {
            new FetchArticleBodyAsyncTask().execute(news_server_hostname, news_server_port, "true");
        }
        else {
            new FetchArticleBodyAsyncTask().execute(news_server_hostname, news_server_port, "false");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private class FetchArticleBodyAsyncTask extends AsyncTask<String, Void, ArticleModel> {
        protected void onPreExecute() {
            // Runs on the UI thread before doInBackground
            // Good for toggling visibility of a progress indicator
            showArticleBodyLoading(true);
        }

        protected ArticleModel doInBackground(String... strings) {
            // Some long-running task like downloading an image.
            try {
                if (Boolean.parseBoolean(strings[2]) || null == mArticle.getBody() || mArticle.getBody().isEmpty()) {
                    if (BuildConfig.DEBUG) {
                        Log.d(NewsgroupListFragment.class.toString(), "Fetching article body from server...");
                    }
                    return new ArticleDetailClient(strings[0], Integer.parseInt(strings[1])).fetch(mArticle);
                }
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Fetching article body locally...");
                }
                return mArticle;
            }
            catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Error while fetching article body from server:");
                }
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(ArticleModel article) {
            // This method is executed in the UIThread
            // with access to the result of the long running task
            if (null != article) {
                mArticle = article;
                super.onPostExecute(article);
                tvSubject.setText(mArticle.getSubject());
                tvFrom.setText(mArticle.getFrom());
                tvDate.setText(mArticle.getDate());

                tvBody.setText(new String(mArticle.getBody().toString().getBytes(), Charset.forName("UTF-8")));
                showArticleBodyLoading(false);
            }
        }
    }
}
