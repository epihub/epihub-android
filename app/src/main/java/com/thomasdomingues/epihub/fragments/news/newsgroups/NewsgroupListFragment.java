package com.thomasdomingues.epihub.fragments.news.newsgroups;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.thomasdomingues.epihub.BuildConfig;
import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.activities.BaseActivity;
import com.thomasdomingues.epihub.adapters.NewsgroupListAdapter;
import com.thomasdomingues.epihub.models.NewsgroupModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class NewsgroupListFragment extends ListFragment {

    private ArrayList<NewsgroupModel> mGroups;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsgroupListFragment() {}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NewsgroupListFragment.
     */
    public static NewsgroupListFragment newInstance() {
        return new NewsgroupListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGroups = new ArrayList<>();
        setListAdapter(new NewsgroupListAdapter(getActivity(), mGroups));

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup parent = (ViewGroup) inflater.inflate(R.layout.fragment_newsgroup_list, container, false);
        parent.addView(v, 0);
        return parent;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Set the adapter
        ListView lv = (ListView) view.findViewById(android.R.id.list);
        TextView emptyText = (TextView) view.findViewById(android.R.id.empty);
        lv.setEmptyView(emptyText);
        lv.setAdapter(getListAdapter());
        fetchSubscribedNewsgroupsAsync();
        getActivity().setTitle(getResources().getString(R.string.news_activity));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.newsgroup_list_menu, menu);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        mListener.onListFragmentInteraction(mGroups.get(position));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment;

        switch (item.getItemId()) {
            case R.id.action_subscribe_to_newsgroups:
                fragment = NewsgroupSubscribeFragment.newInstance();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_content, fragment)
                .addToBackStack(null)
                .commit();
        BaseActivity activity = (BaseActivity) getActivity();
        activity.setDrawerIconEnabled(false);
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        /*swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                fetchTimelineAsync();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        // Construct the data source
        ArrayList<ArticleModel> arrayOfArticles = new ArrayList<ArticleModel>();
        ArticleModel article = new ArticleModel("Thomas Domingues <doming_t@epita.fr>",
                new NewsgroupModel("iit.test"),
                "[IIT][TEST] Test",
                new Date(),
                "This is a dummy content."
        );
        ArticleModel article2 = new ArticleModel("Thomas Domingues <doming_t@epita.fr>",
                new NewsgroupModel("iit.test"),
                "[IIT][TEST] Test",
                new Date(),
                "This is a dummy content."
        );
        arrayOfArticles.add(article);
        arrayOfArticles.add(article2);
        // Create the adapter to convert the array to views
        ArticleListAdapter adapter = new ArticleListAdapter(this, arrayOfArticles);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.lvArticles);
        listView.setAdapter(adapter);*/

        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void fetchSubscribedNewsgroupsAsync() {
        String news_server_hostname = getResources().getString(R.string.news_server_host);
        String news_server_port = getResources().getString(R.string.news_server_port);

        new FetchSubscribedNewsgroupsAsyncTask().execute(news_server_hostname, news_server_port);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(NewsgroupModel group);
    }

    private class FetchNewsgroupsInfoAsyncTask extends AsyncTask<String, Void, ArrayList<NewsgroupModel>> {
        protected void onPreExecute() {
            // Runs on the UI thread before doInBackground
            // Good for toggling visibility of a progress indicator
            setListShown(false);
        }

        protected ArrayList<NewsgroupModel> doInBackground(String... strings) {
            // Some long-running task like downloading an image.
            try {
                List<NewsgroupModel> queryResults = new Select()
                        .from(NewsgroupModel.class)
                        .where("Subscribed = ?", true)
                        .orderBy("Name ASC")
                        .execute();

                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Fetching subscribed newsgroups locally...");
                }
                return (ArrayList<NewsgroupModel>) queryResults;
            }
            catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Error while fetching subscribed newsgroups :");
                    Log.d(NewsgroupListFragment.class.toString(), e.getClass() + " :");
                }
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(ArrayList<NewsgroupModel> groups) {
            // This method is executed in the UIThread
            // with access to the result of the long running task
            if (null != groups) {
                mGroups = groups;
            }
            else {
                mGroups = new ArrayList<>();
            }
            super.onPostExecute(groups);

            NewsgroupListAdapter adapter = (NewsgroupListAdapter) getListAdapter();
            adapter.clear();
            adapter.addAll(mGroups);
            adapter.notifyDataSetChanged();

            setListAdapter(adapter);
            setListShown(true);
            if (BuildConfig.DEBUG) {
                Log.d(NewsgroupListFragment.class.toString(), "Fetched subscribed newsgroups successfully !");
            }
        }
    }

    private class FetchSubscribedNewsgroupsAsyncTask extends AsyncTask<String, Void, ArrayList<NewsgroupModel>> {
        protected void onPreExecute() {
            // Runs on the UI thread before doInBackground
            // Good for toggling visibility of a progress indicator
            setListShown(false);
        }

        protected ArrayList<NewsgroupModel> doInBackground(String... strings) {
            // Some long-running task like downloading an image.
            try {
                List<NewsgroupModel> queryResults = new Select()
                        .from(NewsgroupModel.class)
                        .where("Subscribed = ?", true)
                        .orderBy("Name ASC")
                        .execute();

                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Fetching subscribed newsgroups locally...");
                }
                return (ArrayList<NewsgroupModel>) queryResults;
            }
            catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupListFragment.class.toString(), "Error while fetching subscribed newsgroups :");
                    Log.d(NewsgroupListFragment.class.toString(), e.getClass() + " :");
                }
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(ArrayList<NewsgroupModel> groups) {
            // This method is executed in the UIThread
            // with access to the result of the long running task
            if (null != groups) {
                mGroups = groups;
            }
            else {
                mGroups = new ArrayList<>();
            }
            super.onPostExecute(groups);

            NewsgroupListAdapter adapter = (NewsgroupListAdapter) getListAdapter();
            adapter.clear();
            adapter.addAll(mGroups);
            adapter.notifyDataSetChanged();

            setListAdapter(adapter);
            setListShown(true);
            if (BuildConfig.DEBUG) {
                Log.d(NewsgroupListFragment.class.toString(), "Fetched subscribed newsgroups successfully !");
            }
        }
    }
}
