package com.thomasdomingues.epihub.fragments.news.newsgroups;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.thomasdomingues.epihub.BuildConfig;
import com.thomasdomingues.epihub.R;
import com.thomasdomingues.epihub.adapters.NewsgroupSubscribeAdapter;
import com.thomasdomingues.epihub.adapters.NewsgroupSubscribeAdapter.SubscribedNewsgroupViewHolder;
import com.thomasdomingues.epihub.models.NewsgroupModel;
import com.thomasdomingues.epihub.network.NewsgroupsClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewsgroupSubscribeFragment extends ListFragment {

    private ArrayList<NewsgroupModel> mGroups;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsgroupSubscribeFragment() {}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NewsgroupListFragment.
     */
    public static NewsgroupSubscribeFragment newInstance() {
        return new NewsgroupSubscribeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGroups = new ArrayList<>();
        setListAdapter(new NewsgroupSubscribeAdapter(getActivity(), mGroups));

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup parent = (ViewGroup) inflater.inflate(R.layout.fragment_newsgroup_subscribe, container, false);
        parent.addView(v, 0);
        return parent;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Set the adapter
        ListView lv = (ListView) view.findViewById(android.R.id.list);
        TextView emptyText = (TextView) view.findViewById(android.R.id.empty);
        emptyText.setText("No newsgroups found");
        lv.setEmptyView(emptyText);
        lv.setAdapter(getListAdapter());
        fetchNewsgroupsAsync();
        getActivity().setTitle(getResources().getString(R.string.news_subscribe_title));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.newsgroup_subscribe_menu, menu);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        NewsgroupModel group = mGroups.get(position);
        group.setSubscribed(!group.isSubscribed());
        group.save();
        SubscribedNewsgroupViewHolder snvh = (SubscribedNewsgroupViewHolder) v.getTag();
        snvh.getSubscribe().setChecked(group.isSubscribed());

        /* DEBUG */
        if (BuildConfig.DEBUG) {
            Log.i(NewsgroupSubscribeFragment.class.toString(),
                    "Group " + group.getName() + " subscription: " + group.isSubscribed());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // action home should open or close the drawer
            case R.id.action_validate_subscription:
                getActivity().onBackPressed();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void fetchNewsgroupsAsync() {
        String news_server_hostname = getResources().getString(R.string.news_server_host);
        String news_server_port = getResources().getString(R.string.news_server_port);

        new FetchNewsgroupsAsyncTask().execute(news_server_hostname, news_server_port);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(NewsgroupModel group);
    }

    private class FetchNewsgroupsAsyncTask extends AsyncTask<String, Void, ArrayList<NewsgroupModel>> {
        protected void onPreExecute() {
            // Runs on the UI thread before doInBackground
            // Good for toggling visibility of a progress indicator
            setListShown(false);
        }

        protected ArrayList<NewsgroupModel> doInBackground(String... strings) {
            try {
                // Fetching newsgroup list from server
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupSubscribeFragment.class.toString(), "Fetching newsgroups from server...");
                }

                // Create NNTP Client
                new NewsgroupsClient(strings[0], Integer.parseInt(strings[1])).fetchAll();

                // Get newsgroups locally stored
                List<NewsgroupModel> localGroups =  new Select().from(NewsgroupModel.class).orderBy("Name ASC")
                        .execute();

                // Return local groups
                return (ArrayList<NewsgroupModel>) localGroups;
            }
            catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    Log.d(NewsgroupSubscribeFragment.class.toString(), "Error while fetching newsgroups :");
                }
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(ArrayList<NewsgroupModel> groups) {
            // This method is executed in the UIThread
            // with access to the result of the long running task
            if (null != groups) {
                mGroups = groups;
            }
            else {
                mGroups = new ArrayList<>();
            }
            super.onPostExecute(groups);

            // Replace previous data set with newly fetched data set
            NewsgroupSubscribeAdapter adapter = (NewsgroupSubscribeAdapter) getListAdapter();
            adapter.clear();
            adapter.addAll(mGroups);
            adapter.notifyDataSetChanged();

            // Put items in ListView
            setListAdapter(adapter);
            setListShown(true);
            if (BuildConfig.DEBUG) {
                Log.d(NewsgroupSubscribeFragment.class.toString(), "Fetched newsgroups from server successfully !");
            }
        }
    }
}
