package com.thomasdomingues.epihub;

import android.os.Build;

import org.apache.commons.net.nntp.NNTPClient;
import org.apache.commons.net.nntp.NewsgroupInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.io.IOException;
import java.util.ArrayList;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class NNTPClientTest {
    @Before
    public void setup() {
        ShadowLog.stream = System.out;
    }

    @Test
    public void nntpClient_checkServerConnection_isOk() {
        boolean test = true;
        NNTPClient client = new NNTPClient();
        try {
            client.connect("news.epita.fr", 119);
        }
        catch (IOException e) {
            test = false;
        }

        assertTrue(test);
    }

    @Test
    public void nntpClient_getServerNewsgroups_isOk() {
        boolean test = true;
        NNTPClient client = new NNTPClient();
        int i = 0;
        ArrayList<String> list = new ArrayList<>();

        try {
            client.connect("news.epita.fr", 119);

            try {
                for (NewsgroupInfo n : client.iterateNewsgroups("")) {
                    ++i;
                    list.add(n.getNewsgroup());
                }
            }
            catch (IOException e) {
                test = false;
            }
        }
        catch (IOException e) {
            test = false;
        }
        finally {
            try {
                if (client.isConnected()) {
                    client.disconnect();
                }
            }
            catch (IOException e) {
                test = false;
            }
        }

        ShadowLog.i(this.getClass().toString(), i + " newsgroups found!");
        ShadowLog.i(this.getClass().toString(), list.toString());

        assertTrue(test);

        assertTrue(list.contains("iit.test"));
        assertTrue(list.contains("epita.c"));
        assertFalse(list.contains("false.test"));
    }

    @Test
    public void nntpClient_getNewNews_isOk() {
        boolean test = true;
        NNTPClient client = new NNTPClient();
        long remote, local;
        // ArrayList<String> list = new ArrayList<>();

        try {
            client.connect("news.epita.fr", 119);

            NewsgroupInfo group = new NewsgroupInfo();
            client.selectNewsgroup("epita.assistants", group);

            remote = group.getArticleCountLong();
            local = (long) (Math.random() * (remote + 1));

            ShadowLog.i(this.getClass().toString(), "(REMOTE) Number of articles : " + remote);
            ShadowLog.i(this.getClass().toString(), "(LOCAL) Number of articles : " + local);

            try {
                ShadowLog.i(this.getClass().toString(), "Testing...");
            }
            catch (Exception e) {
                test = false;
            }
        }
        catch (IOException e) {
            test = false;
        }
        finally {
            try {
                if (client.isConnected()) {
                    client.disconnect();
                }
            }
            catch (IOException e) {
                test = false;
            }
        }

        assertTrue(test);
    }
}
